# Tidy Tuesday Showcase

In Spring 2018 I discoverd Tidy Tuesday, a weekly data project in R. Every week a new data set is posted on the [Tidy Tuesday Github page](https://github.com/rfordatascience/tidytuesday). Anyone can grab the data and display any interesting insights or patterns in graphs. You can then post your graph on Twitter, accompanied by the #TidyTuesday hashtag and a link to your code. It's a great way to train your R and ggplot skills, learn about new packages, and learn cool new tricks from others in the online R community.  

Learn more about Tidy Tuesday on their [Github page](https://github.com/rfordatascience/tidytuesday). If you want to know more about my personal experience with Tidy Tuesday and what you can learn from participating, read the [blog post](https://surroundedbydata.netlify.app/post/tidy-tuesday/) I wrote about Tidy Tuesday on [my website](https://surroundedbydata.netlify.app/). 


<br/>

**A selection of Tidy Tuesday graphs I created:**

- [Most horror movies are released in October](#most-horror-movies-are-released-in-october)
- [2019 Women's World Cup](#2019-womens-world-cup)
- [A pie chart of Thanksgiving pies](#a-pie-chart-of-thanksgiving-pies)
- [R-downloads in Europe](#r-downloads-in-europe)
- [Babies born during public holidays](#babies-born-during-public-holidays)
- [Cats and dogs in the US](#cats-and-dogs-in-the-us)
- [Calories in salads from fastfood chains](#fastfood-calories)
- [Global alcohol consumption](#global-alcohol-consumption)
- [2010 FIFA World Cup Audience](#2010-fifa-world-cup-audience)


<br/><br/>


## Most horror movies are released in October

*October 22, 2019*
<br/>

In many countries, horror movies are released in October, in anticipation of Halloween. Malaysia looks like an interesting exception. I learned to annotate by drawing the arrows and adding text. Also, I think the dark background fits the graph quite well :) The data can be found [here](https://github.com/rfordatascience/tidytuesday/tree/master/data/2019/2019-10-22), and my code for the graph [here](https://gitlab.com/snippets/1907498).

<br/>

<center>
<img src="Plots/HorrorMovies.jpg" width="750">
</center>

<br/>



## 2019 Women's World Cup

*July 9, 2019*
<br/>

During the 2019 Women's World Cup (soccer), the final was between the Netherlands and the US. For both countries, I looked at the top 10 players with most goals scored. I added a portrait of each squads top scorer with [annotation_custom()](https://ggplot2.tidyverse.org/reference/annotation_custom.html). The ball symbol was added using the [emo package](https://github.com/hadley/emo). The Tidy Tuesday dataset can be found [here](https://github.com/rfordatascience/tidytuesday/tree/master/data/2019/2019-07-09), but as it was not updated to include the 2019 World Cup results, I took data directly from the websites of the Dutch and US teams. My code can be found [here](https://gitlab.com/snippets/1875013).


<center>
<img src="Plots/Top10_goals_NL.jpg" width="750">
</center>

<center>
<img src="Plots/Top10_goals_US.jpg" width="750">
</center>

<br/>


## A pie chart of Thanksgiving pies

*November 20, 2018*
<br/>

I know some people think pie charts are evil, but I simply **had** to use it to display the types of pies that appear at Thanksgiving dinner. It turns out pumpkin pie is America's favorite pie, with 34% of household serving it during Thanksgiving dinner. The data can be found [here](https://github.com/rfordatascience/tidytuesday/tree/master/data/2018/2018-11-20), and my code [here](https://gitlab.com/snippets/1781798).

<center>
<img src="Plots/PieChart2.jpg" width="500">
</center>


## R downloads in Europe

*October 30, 2018*
<br/>

The data is about R downloads worldwide. I created a heatmap showing number of downloads per day of the week and hour of the day, selecting only European countries. Tuesdays between 11 and 12 AM have the highest number of people downloading R from CRAN. Relatively few people download R during weekends. I think it's great that you can see a clear pattern during work days, including a lower number of downloads during the lunch break :D The data can be found [here](https://github.com/rfordatascience/tidytuesday/tree/master/data/2018/2018-10-30), and the code for this heatmap [here](https://gitlab.com/snippets/1774186).

<center>
<img src="Plots/R_downloads_week_Europe.jpg" width="650">
</center>

<br/>





## Babies born during public holidays

*October 2, 2018*
<br/>

On public holidays, are there more or less babies being born than on other days? Some holidays are not very different from regular weekdays, but on others the birth rate is much lower. Christmas is the public holiday with the lowest birth rate. The data can be found [here](https://github.com/rfordatascience/tidytuesday/tree/master/data/2018/2018-10-02), and the code for this map [here](https://gitlab.com/snippets/1759511).

<center>
<img src="Plots/birthsDuringHolidays_plot.jpg" width="650">
</center>

<br/>



## Cats and dogs in the US

*September 11, 2018*
<br/>

Are you a dog person or a cat person? Cats and dogs are not evenly distributed over the US. Cat people are more often found in the north, while people with dogs more often live in the south of the US. Find the data [here](https://github.com/rfordatascience/tidytuesday/tree/master/data/2018/2018-09-11), and the code for this map [here](https://gitlab.com/snippets/1753053).

<center>
<img src="Plots/catdog_us.jpg" width="650">
</center>

<br/>




## Fastfood calories

*September 4, 2018*
<br/>

Not all salads from fastfood chains are created equal... The average Taco Bell salad contains about three times as many calories as the average Subway salad. The data can be found in the [Tidy Tuesday repository](https://github.com/rfordatascience/tidytuesday/tree/master/data/2018/2018-09-04) and the code for creating this graph [here](https://gitlab.com/snippets/1751485).

<center>
<img src="Plots/saladcalories.jpg" width="650">
</center>

<br/>



## Global alcohol consumption

*June 26, 2018*
<br/>

The data contains info about how much wine, beer and spirits are being consumed in countries around the world. From this, I gleaned which type of alcohol is the most popular in each country. I created a world map and a map of Europe. The data for these maps can be found [here](https://github.com/rfordatascience/tidytuesday/tree/master/data/2018/2018-06-26), the code can be found [here](https://gitlab.com/snippets/1728754).

<center>
<img src="Plots/Preferred_worldmap.png" width="750">
<img src="Plots/Preferred_Europe.png" width="550">
</center>

<br/>


## 2010 FIFA World Cup Audience

*June 12, 2018*
<br/>

For this Tidy Tuesday, the data was about the 2010 FIFA World Cup (soccer) and the number of viewers in each country. In countries with higher FIFA rankings, a larger proportion of the population watched the 2010 World Cup. This is one of the first times I used [ggrepel](https://cran.r-project.org/web/packages/ggrepel/vignettes/ggrepel.html) for annotating points. You can find the data [here](https://github.com/rfordatascience/tidytuesday/tree/master/data/2018/2018-06-12) and the code used to create this graph [here](https://gitlab.com/snippets/1723983).

<center>
<img src="Plots/AudienceRanking.jpg" width="650">
</center>




